﻿/* 
	humanstxt.org 
	The humans responsible & technology colophon
*/

/* TEAM */
Your title: team.
Site: email, link to a contact form, etc. (Prepare yourself to get spammed if you add an email address)
Twitter: twitter.
Location: Bishkek, Kyrgyzstan.

/* THANKS */
Name: people

/* SITE */
Last update: YYYY/MM/DD (Updating this could get annoying)
Standards: HTML5, CSS3,.. (This is not a good idea from a security standpoint)
Components: Modernizr, jQuery, etc. (This is not a good idea from a security standpoint)
Software: Software used for the development (This is not a good idea from a security standpoint)