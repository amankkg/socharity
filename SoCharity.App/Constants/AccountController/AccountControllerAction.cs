﻿namespace SoCharity.App.Constants
{
    public static class AccountControllerAction
    {
        public const string Login = "Login";

        public const string VerifyCode = "VerifyCode";

        public const string Register = "Register";

        public const string ConfirmEmail = "ConfirmEmail";

        public const string ForgotPassword = "ForgotPassword";

        public const string ForgotPasswordConfirmation = "ForgotPasswordConfirmation";

        public const string ResetPassword = "ResetPassword";

        public const string ResetPasswordConfirmation = "ResetPasswordConfirmation";

        public const string ExternalLogin = "ExternalLogin";

        public const string SendCode = "SendCode";

        public const string ExternalLoginCallback = "ExternalLoginCallback";

        public const string ExternalLoginConfirmation = "ExternalLoginConfirmation";

        public const string LogOff = "LogOff";

        public const string ExternalLoginFailure = "ExternalLoginFailure";
    }
}
