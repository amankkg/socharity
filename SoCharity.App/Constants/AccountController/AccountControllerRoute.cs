﻿namespace SoCharity.App.Constants
{
    public static class AccountControllerRoute
    {
        public const string GetAccount = ControllerName.Account + "GetAccount";
        public const string GetVerifyCode = ControllerName.Account + "GetVerifyCode";
        public const string GetRegister = ControllerName.Account + "GetRegister";
        public const string GetConfirmEmail = ControllerName.Account + "GetConfirmEmail";
        public const string GetForgotPassword = ControllerName.Account + "GetForgotPassword";
        public const string GetForgotPasswordConfirmation = ControllerName.Account + "GetForgotPasswordConfirmation";
        public const string GetResetPassword = ControllerName.Account + "GetResetPassword";
        public const string GetResetPasswordConfirmation = ControllerName.Account + "GetResetPasswordConfirmation";
        public const string GetExternalLogin = ControllerName.Account + "GetExternalLogin";
        public const string GetSendCode = ControllerName.Account + "GetSendCode";
        public const string GetExternalLoginCallback = ControllerName.Account + "GetExternalLoginCallback";
        public const string GetExternalLoginConfirmation = ControllerName.Account + "GetExternalLoginConfirmation";
        public const string GetLogOff = ControllerName.Account + "GetLogOff";
        public const string GetExternalLoginFailure = ControllerName.Account + "GetExternalLoginFailure";
    }
}
