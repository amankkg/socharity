﻿namespace SoCharity.App.Constants
{
    public class TopicsControllerAction
    {
        public const string Index = "Index";

        public const string Create = "Create";

        public const string Edit = "Edit";

        public const string Delete = "Delete";

        public const string Contribute = "Contribute";
    }
}