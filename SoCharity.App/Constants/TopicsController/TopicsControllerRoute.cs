﻿namespace SoCharity.App.Constants
{
    public class TopicsControllerRoute
    {
        public const string GetIndex = ControllerName.Topics + "GetIndex";
        public const string GetCreate = ControllerName.Topics + "GetCreate";
        public const string PostCreate = ControllerName.Topics + "PostCreate";
        public const string GetEdit = ControllerName.Topics + "GetEdit";
        public const string PostEdit = ControllerName.Topics + "PostEdit";
        public const string GetDelete = ControllerName.Topics + "GetDelete";
        public const string PostContribute = ControllerName.Topics + "PostContribute";
    }
}