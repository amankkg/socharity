﻿namespace SoCharity.App.Constants
{
    public class Application
    {
        public const string Name = "SoCharity.org";
        public const string ShortName = "SoCharity";
    }
}