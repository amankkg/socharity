﻿namespace SoCharity.App.Constants
{
    public static class ControllerName
    {
        public const string Error = "Error";
        public const string Home = "Home";
        public const string Account = "Account";
        public const string Topics = "Topics";
        public const string Cabinet = "Cabinet";
    }
}