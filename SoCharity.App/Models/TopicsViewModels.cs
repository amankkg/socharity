﻿namespace SoCharity.App.Models
{
    public class DonationSubmitModel
    {
        public int TopicId { get; set; }
        public string FbPostId { get; set; }
    }
}
