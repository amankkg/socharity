﻿using System;
using System.Data.Entity;
using System.Linq;

namespace SoCharity.App.Models
{
    public class Topic
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
        public string FacebookUrl { get; set; }
        public DateTime Created { get; set; }

        public virtual IQueryable<Charity> Chartities { get; set; }
    }

    public class Charity
    {
        public int Id { get; set; }
        public int TopicId { get; set; }
        public string UserId { get; set; }
        public string FbPostId { get; set; }
        public DateTime Created { get; set; }

        public virtual Topic Topic { get; set; }
    }

    public class DomainDbContext : DbContext
    {
        public DomainDbContext()
            : base("DomainDbConnection")
        {
        }

        public static DomainDbContext Create()
        {
            return new DomainDbContext();
        }

        public DbSet<Topic> Topics { get; set; }
        public DbSet<Charity> Charities { get; set; }
    }
}
