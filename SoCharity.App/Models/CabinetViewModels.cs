﻿using System.Collections.Generic;

namespace SoCharity.App.Models
{
    public class CabinetIndexViewModel
    {
        public ApplicationUser User { get; set; }
        public IEnumerable<Topic> Topics { get; set; }
        public IEnumerable<Charity> Charities { get; set; }
    }
}
