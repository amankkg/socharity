﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SoCharity.App.Constants;
using SoCharity.App.Models;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace SoCharity.App.Controllers
{
    [Authorize]
    public class CabinetController : Controller
    {
        private DomainDbContext _db;

        public CabinetController()
        {
            _db = new DomainDbContext();
        }

        [Route("cabinet/index", Name = CabinetControllerRoute.GetIndex)]
        public ActionResult Index()
        {
            var uid = User.Identity.GetUserId();
            var user = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>().FindByIdAsync(uid).Result;
            var charities = from c in _db.Charities
                            where c.UserId == uid
                            orderby c.Id descending
                            select c;
            var topics = from c in charities
                         select c.Topic;
            var model = new CabinetIndexViewModel
            {
                User = user,
                Topics = topics,
                Charities = charities
            };
            return View(model);
        }
    }
}
