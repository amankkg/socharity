﻿namespace SoCharity.App.Controllers
{
    using Constants;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Models;
    using System;
    using System.Linq;
    using System.Net;
    using System.Web;
    using System.Web.Mvc;

    public class TopicsController : Controller
    {
        private ApplicationUserManager _userManager;
        private DomainDbContext _db;

        private ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ??
                    (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());
            }
        }
        private DomainDbContext Db
        {
            get
            {
                return _db ??
                    (_db = HttpContext.GetOwinContext().Get<DomainDbContext>());
            }
        }

        [Route("", Name = TopicsControllerRoute.GetIndex)]
        public ActionResult Index(int take = 5, int skip = 0)
        {
            var model = (from t in Db.Topics
                         orderby t.Id descending
                         select t).Skip(skip).Take(take);
            return View(TopicsControllerAction.Index, model);
        }

        [Route("create", Name = TopicsControllerRoute.GetCreate)]
        public ActionResult Create()
        {
            var model = new Topic();
            return View(TopicsControllerAction.Create, model);
        }

        [HttpPost]
        [Route("create", Name = TopicsControllerRoute.PostCreate)]
        public ActionResult Create(Topic model)
        {
            model.Created = DateTime.Now;
            Db.Topics.Add(model);
            Db.SaveChanges();
            return RedirectToAction(TopicsControllerAction.Index);
        }

        [Route("edit", Name = TopicsControllerRoute.GetEdit)]
        public ActionResult Edit(int id)
        {
            var model = Db.Topics.Find(id);
            return View(TopicsControllerAction.Edit, model);
        }

        [HttpPost]
        [Route("edit", Name = TopicsControllerRoute.PostEdit)]
        public ActionResult Edit(int id, Topic model)
        {
            var target = Db.Topics.Find(id);
            target.Caption = model.Caption;
            target.Message = model.Message;
            target.Url = model.Url;
            target.FacebookUrl = model.FacebookUrl;
            Db.SaveChanges();
            return RedirectToAction(TopicsControllerAction.Index);
        }

        [HttpGet]
        [Route("delete", Name = TopicsControllerRoute.GetDelete)]
        public ActionResult Delete(int id)
        {
            var target = Db.Topics.Find(id);
            if (target != null)
            {
                Db.Topics.Remove(target);
                Db.SaveChanges();
                return RedirectToAction(TopicsControllerAction.Index);
            }
            return HttpNotFound();
        }

        [HttpPost]
        [Route("contribute", Name = TopicsControllerRoute.PostContribute)]
        public ActionResult Contribute(DonationSubmitModel model)
        {
            var topic = Db.Topics.Find(model.TopicId);
            var uid = User.Identity.GetUserId();
            Db.Charities.Add(new Charity
            {
                Created = DateTime.Now,
                TopicId = topic.Id,
                UserId = uid,
                FbPostId = model.FbPostId
            });
            Db.SaveChanges();
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}