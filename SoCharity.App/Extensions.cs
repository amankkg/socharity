﻿using System.Web.Mvc;

namespace SoCharity.App
{
    public static class Extensions
    {
        /// <summary>
        /// C# compiler preprocessor logic for DEBUG/RELEASE cases, i.e. returns true if DEBUG
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public static bool IsDebug(this HtmlHelper htmlHelper)
        {
            return
#if DEBUG
            true;
#else
            false;
#endif
        }
    }
}
