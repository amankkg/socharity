namespace SoCharity.App.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCharities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Charities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TopicId = c.Int(nullable: false),
                        UserId = c.String(),
                        FbPostId = c.String(),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Topics", t => t.TopicId, cascadeDelete: true)
                .Index(t => t.TopicId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Charities", "TopicId", "dbo.Topics");
            DropIndex("dbo.Charities", new[] { "TopicId" });
            DropTable("dbo.Charities");
        }
    }
}
